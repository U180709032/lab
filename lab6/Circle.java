public class Circle {

    int radius ;
    Point centre ;


    public Circle(int radius, Point centre) {
        this.radius = radius;
        this.centre = centre;
    }

    public double area () {
        return Math.PI * radius * radius ;

    }

    public double perimeter () {
        return Math.PI * 2 * radius ;

    }

    public boolean intersect (Circle c) {
        if (radius + c.radius > centre.distance(c.centre))
            return true ;
        else
            return false ;
    }


}
