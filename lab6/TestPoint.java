public class TestPoint {

    public static void main(String[] args) {
        Point p1 = new Point() ;
        System.out.println("x = " + p1.xCoord + " y = " + p1.yCoord );
        p1.xCoord = 3 ;
        p1.yCoord = 2 ;
        System.out.println("x = " + p1.xCoord + " y = " + p1.yCoord );

        Point p2 = new Point() ;
        p2.xCoord = 5 ;
        p2.yCoord = 6 ;
        System.out.println("P2 x = " + p2.xCoord + " P2 y = " + p2.yCoord);

        System.out.println("distance p1 to p2 " + p1.distance(p2));
        System.out.println("distance p2 to p1 " + p2.distance(p1));



        Point p3 = new Point(5,6) ;
        System.out.println("P3 x = " + p3.xCoord + " P3 y = " + p3.yCoord );



    }
}
