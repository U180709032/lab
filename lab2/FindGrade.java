public class FindGrade {
    public static void main(String[] args) {
        int score = Integer.parseInt(args[0]);

        if (score > 100 || score < 0)
            System.out.println("It ıs not  valid score ");
        else if (score >= 90) {
            System.out.println("your grade is AA");
        } else if (score >= 80) {
            System.out.println("your grade is BB");
        } else if (score >= 70) {
            System.out.println("your grade is CC");
        } else if (score >= 60) {
            System.out.println("your grade is DD");
        } else {
            System.out.println("your grade is FF");
        }
    }
}