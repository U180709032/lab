public class FindPrimes {
    public static void main(String[] args) {

        //get the number
        int given = Integer.parseInt(args[0]);

        //for each number less than given
        for (int number = 2; number < given; number++) {
            boolean isPrime = true;
            //for each divisor less than number
            for (int divisor = 2; divisor < number; divisor++) {

                //if number is divisible by divisor
                if (number % divisor == 0) {
                    isPrime = false;
                    break;
                }
            }
            //isPrime is false

            //end the loop

            if (isPrime) {
                //if number is prime
                System.out.print(number + " , ");
            }  //print the number
        }
    }
}
